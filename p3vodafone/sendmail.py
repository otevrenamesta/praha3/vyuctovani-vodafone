#!/usr/bin/env python3
import atexit
import os
import smtplib
import time
from base64 import b64encode, b64decode
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Optional, Tuple, Callable

import profig
import requests
from appdirs import user_config_dir
from msal import PublicClientApplication, SerializableTokenCache


class SendmailTokenCache(SerializableTokenCache):
    CONFIG_KEY_CACHE: str = "token.cache"

    def __init__(self):
        super().__init__()
        self.config_filename: str = "o365.cache.ini"
        self.config_location: str = user_config_dir(appname="P3Vodafone", appauthor="OtevrenaMesta")
        self.config: profig.Config = profig.Config(
            os.path.join(self.config_location, self.config_filename)
        )
        self.config.init(self.CONFIG_KEY_CACHE, "")
        self.config.sync()

    def deserialize(self, state: Optional[str]) -> None:
        cache_data: str = self.config.get(self.CONFIG_KEY_CACHE) or b""
        super().deserialize(state=b64decode(cache_data).decode('utf8'))

    def serialize(self) -> str:
        cache: str = super().serialize()
        self.config[self.CONFIG_KEY_CACHE] = str(b64encode(cache.encode('utf8')), "utf8")
        self.config.sync()
        return cache


class VyuctovaniSender:
    def __init__(
            self,
            use_ms_graph_api: bool = True
    ):
        self.method = "o365" if use_ms_graph_api else "smtp"

    def mail(
            self,
            target_email: str,
            email_subject: str,
            email_text: str,
            attach_file_name_absolute_path: str,
            source_email: str = None,
    ) -> Tuple[bool, Optional[str]]:
        # check attachment requirement
        if not os.path.exists(attach_file_name_absolute_path):
            return False, f"Soubor přílohy neexistuje {attach_file_name_absolute_path}"
        if not os.access(attach_file_name_absolute_path, os.R_OK):
            return False, f"Soubor s přílohou nelze otevřít {attach_file_name_absolute_path}"
        # remove whitespaces
        target_email = target_email.strip()
        source_email = source_email.strip()
        email_text = email_text.strip()
        email_subject = email_subject.strip()
        # call appropriate sender method
        if self.method == "o365":
            return self.o365(
                target_email=target_email,
                email_subject=email_subject,
                email_text=email_text,
                attach_file_name_absolute_path=attach_file_name_absolute_path,
                source_email=source_email,
            )
        else:
            return self.smtp(
                target_email=target_email,
                email_subject=email_subject,
                email_text=email_text,
                attach_file_name_absolute_path=attach_file_name_absolute_path,
                source_email=source_email
            )

    @staticmethod
    def o365(
            target_email: str,
            email_subject: str,
            email_text: str,
            source_email: str,
            attach_file_name_absolute_path: str,
    ) -> Tuple[bool, Optional[str]]:
        scopes: list[str] = ["User.Read", "Mail.Send", "Mail.Send.Shared"]
        token_cache: SendmailTokenCache = SendmailTokenCache()
        token_cache.deserialize(state=None)

        atexit.register(lambda: token_cache.serialize())

        app: PublicClientApplication = PublicClientApplication(
            client_id="6fe84237-201e-4656-b651-0c1d9309b5b7",
            authority="https://login.microsoftonline.com/praha3.cz",
            token_cache=token_cache
        )

        result: Optional[dict] = None
        accounts = app.get_accounts()
        if accounts:
            chosen = accounts[0]
            result = app.acquire_token_silent(
                scopes=scopes,
                account=chosen,
            )

        if not result:
            result = app.acquire_token_interactive(
                scopes=scopes,
                prompt="login",
                timeout=180,
                domain_hint="praha3.cz",
            )

        if "access_token" not in result:
            return False, f"Chyba přihlášení\nChyba: {result.get('error', '')}\nPopis chyby: {result.get('error_description', '')}"

        # Explicit cache save after result is success
        token_cache.serialize()

        if result.get("id_token_claims"):
            print("Přihlášení úspěšné pro: %s (%s)" % (
                result.get("id_token_claims").get("preferred_username"), result.get("id_token_claims").get("name")
            ))
            token_email = result.get("id_token_claims").get("preferred_username")
            if not source_email.strip() and token_email:
                # Pokud nebyl poskytnut e-mail odesílatele, použít e-mail přihlášeného uživatele
                source_email = token_email
        else:
            print("Používám cache posledního přihlášení")

        req_dict: dict = {
            "message": {
                "subject": f"{email_subject}",
                "body": {
                    "contentType": "Text",
                    "content": f"{email_text}"
                },
                "toRecipients": [
                    {
                        "emailAddress": {
                            "address": f"{target_email}",
                        },
                    }
                ],
                "from": {
                    "emailAddress": {
                        "address": f"{source_email}"
                    }
                },
                "attachments": [
                    {
                        "@odata.type": "#microsoft.graph.fileAttachment",
                        "name": "vyúčtování.pdf",
                        "contentType": "application/pdf",
                        "contentBytes": b64encode(open(attach_file_name_absolute_path, "rb").read()).decode('ascii')
                    }
                ]
            }
        }
        graph_api_headers: dict = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {result['access_token']}"
        }

        def do_send(json: dict, headers: dict) -> Callable[[], requests.Response]:
            return lambda: requests.post(
                url="https://graph.microsoft.com/v1.0/me/sendMail",
                json=json,
                headers=headers,
            )

        call_send = do_send(json=req_dict, headers=graph_api_headers)
        post_result: requests.Response = call_send()

        if post_result.status_code == 429:
            sleep_time: float = float(post_result.headers.get('Retry-After') or 0)
            print(f"429 Throttle hit, sleep for: {sleep_time} seconds")
            time.sleep(sleep_time)
            post_result = call_send()

        if post_result.status_code == 202:
            return True, "Email byl úspěšně odeslán pomocí MS Graph API"

        error_message: str = "E-mail nebyl odeslán, neznámá chyba"
        if "json" in post_result.headers['Content-Type']:
            parsed = post_result.json()
            if isinstance(parsed, dict) and "error" in parsed:
                error_message = parsed.get('error').get('message')

        # append http status code for clarity
        error_message = f"{error_message} (HTTP {post_result.status_code})"

        return False, error_message

    @staticmethod
    def smtp(
            target_email: str,
            email_subject: str,
            email_text: str,
            attach_file_name_absolute_path: str,
            source_email: str = None,
    ) -> Tuple[bool, Optional[str]]:
        """

        :param target_email: cílová e-mailová adresa v jakékoli doméně
        :param email_subject: předmět emailu
        :param email_text: text emailu (text/plain)
        :param attach_file_name_absolute_path: absolutní cesta k souboru PDF přílohy
        :param source_email: e-mail odesílatele (aktuálně bez autentizace)
        :return: Tuple [boolean výsledek operace, str|None chybová hláška]
        """
        msg = MIMEMultipart('alternative')
        msg['Subject'] = email_subject
        msg['From'] = source_email
        msg['To'] = target_email

        part1 = MIMEText(email_text, 'plain')
        msg.attach(part1)

        with open(attach_file_name_absolute_path, 'rb') as attachment_file:
            attachment_name = os.path.basename(attach_file_name_absolute_path)

            part = MIMEApplication(attachment_file.read(), _subtype='pdf')
            part.add_header(_name="Content-Disposition", _value="attachment", filename=attachment_name)
            msg.attach(part)

        with smtplib.SMTP(host="mailp3c.praha3.cz", port=25) as smtp_session:
            smtp_session.set_debuglevel(2)
            smtp_session.ehlo()
            try:
                smtp_session.sendmail(source_email, target_email, msg.as_string())
            except smtplib.SMTPResponseException as e:
                return False, e.smtp_error
            smtp_session.quit()

        return True, f"E-mail byl odeslán pro {target_email}"


if __name__ == '__main__':
    counter: int = 0
    while True:
        operation_result, error_or_message = VyuctovaniSender(use_ms_graph_api=True).mail(
            target_email="marek.sebera@gmail.com",
            source_email="telefony@praha3.cz",
            email_subject=f"Předmět e-mailu {counter}",
            email_text="Nějaký\nVíceřádkový\ns diakritikou\textový\nemail",
            attach_file_name_absolute_path="/tmp/vyuctovani.pdf",
        )
        if not operation_result:
            print("Chyba")
            print(error_or_message)
            break
        elif counter > 100:
            print("Hit 100 sent, break")
            break
        else:
            print(f"Odesláno č. {counter}")
            print(f"Výsledek {error_or_message}")
            counter += 1
